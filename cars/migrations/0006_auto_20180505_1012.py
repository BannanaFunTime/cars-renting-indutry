# Generated by Django 2.0.3 on 2018-05-05 10:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cars', '0005_auto_20180505_1010'),
    ]

    operations = [
        migrations.AlterField(
            model_name='city',
            name='lat',
            field=models.DecimalField(decimal_places=7, max_digits=10),
        ),
        migrations.AlterField(
            model_name='city',
            name='lon',
            field=models.DecimalField(decimal_places=7, max_digits=10),
        ),
    ]
