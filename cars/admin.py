from django.contrib import admin
from .models import RentingProcess, Auto, City
# Register your models here.


admin.site.register(Auto)
admin.site.register(RentingProcess)
admin.site.register(City)
