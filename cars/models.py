from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

# Create your models here.


class City(models.Model):
    lat = models.DecimalField(max_digits=10, decimal_places=7, null=False)
    lon = models.DecimalField(max_digits=10, decimal_places=7, null=False)
    city_name = models.CharField(max_length=40, null=False)

    class Meta:
        ordering = ('city_name',)
        verbose_name = 'city',
        verbose_name_plural = 'cities'

    def __str__(self):
        return self.city_name

    def get_coordinates(self):
        return [self.lat, self.lon]


class Auto(models.Model):
    name = models.CharField(max_length=255, db_index=True)
    description = models.CharField(max_length=255)
    lat = models.DecimalField(max_digits=10, decimal_places=7, null=False)
    lon = models.DecimalField(max_digits=10, decimal_places=7, null=False)
    image = models.ImageField(upload_to='autos/%Y/%m/%D/', blank=True, max_length=2000)
    status = models.BooleanField(default=True) #status zajętości jeśli true to wolne
    city = models.ForeignKey(City, on_delete=models.CASCADE, related_name='cities', default=None)

    class Meta:
        ordering = ('name',)
        verbose_name = 'auto',
        verbose_name_plural = 'autos'

    def __str__(self):
        return self.name

    def get_coordinates(self):
        return [self.lat, self.lon]


class RentingProcess(models.Model):
    RENT_TARIFF = 50.0
    RENT_PENALTY_TARIFF = 200.0

    auto = models.ForeignKey(Auto, on_delete=models.CASCADE, related_name='autos')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='users')
    lending_timestamp = models.DateTimeField(null=False)
    returning_timestamp = models.DateTimeField(null=False)
    real_returning_timestamp = models.DateTimeField(null=True)
    paid = models.IntegerField(null=True)
    once = models.BooleanField(default=False)

    class Meta:
        ordering = ('id',)
        verbose_name = 'rent',
        verbose_name_plural = 'rents'

    def calculate_payment(self):
        total_seconds = (timezone.now() - self.lending_timestamp).total_seconds()
        return int(total_seconds / 3600 * self.RENT_TARIFF)

    def calculate_penalty(self):
        total_seconds = (timezone.now() - self.returning_timestamp).total_seconds()
        return int(total_seconds / 3600 * self.RENT_PENALTY_TARIFF)


# class HistoricPositions(models.Model):
#     auto = models.ForeignKey(Auto, on_delete=models.CASCADE, related_name='autos')
#     lat = models.DecimalField(max_digits=9, decimal_places=6, null=False)
#     lon = models.DecimalField(max_digits=9, decimal_places=6, null=False)
