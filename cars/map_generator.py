from .models import Auto, City
import folium


class MapGenerator(object):
    def __init__(self, city_name):
        self._city = self._get_city_coordinates(city_name)
        self._car_positions = self._get_cars_positions(self._city['name'])
        self._map = self._generate_map()

    @staticmethod
    def _get_city_coordinates(city_name):
        city = City.objects.get(city_name=city_name)
        if not city:
            return {
                'coordinates': None,
                'name': None
            }

        result = {
            'coordinates': city.get_coordinates(),
            'name': city_name
        }
        return result

    def get_map(self):
        return self._map

    @staticmethod
    def _get_cars_positions(city_name):
        if not city_name:
            return []

        positions = [(car.get_coordinates(), car.status) for car in Auto.objects.filter(city__city_name=city_name)]
        return positions

    def _generate_map(self):
        try:
            map_osm = folium.Map(
                location=self._city['coordinates'],
                min_zoom=12,
                zoom_start=13,
            )

            return map_osm

        except Exception as exp:
            print(exp)
            return None

    def update_markers(self):
        for car_position in self._car_positions:
            car_details = Auto.objects.filter(lat=car_position[0][0],
                                              lon=car_position[0][1])

            folium.Marker(car_position[0],
                          popup='{0} - Dostępne'.format(car_details[0].name) if car_position[1] is True else '{0} - Niedostępne'.format(car_details[0].name),
                          icon=folium.Icon(color='green', icon='plus-sign') if car_position[1] is True
                          else folium.Icon(color='red', icon='remove-sign')
                          ).add_to(self._map)

    def save_adjsuted_map(self, car_id=None):
        if car_id:
            self._map.save('./media/html_maps/{0}'.format('car_{0}_map.html').format(car_id))
        else:
            self._map.save('./media/html_maps/map.html')


class CarPositionGenerator(MapGenerator):
    def __init__(self, car_id, city_name):
        super(CarPositionGenerator, self).__init__(city_name=city_name)
        self._car_id = car_id
    # trzeba sprawdzić jak wywołać konstruktor wcześniej wywołaej metody

    def mark_car_position(self):
        car = Auto.objects.get(id=self._car_id)
        car_position_details = {
            'coordinates': car.get_coordinates(),
            'status': car.status
        }
        self._map.zoom_start = 16
        self._map.location = car_position_details['coordinates']
        car_details = Auto.objects.filter(lat=car_position_details['coordinates'][0],
                                          lon=car_position_details['coordinates'][1])

        folium.Marker(car_position_details['coordinates'],
                      popup='{0} - Dostępne'.format(car_details[0].name) if car_position_details['status'] is True else '{0} - Niedostępne'.format(car_details[0].name),
                      icon=folium.Icon(color='green', icon='plus-sign') if car_position_details['status'] is True
                      else folium.Icon(color='red', icon='remove-sign')
                      ).add_to(self._map)

        self.save_adjsuted_map(car_id=self._car_id)
