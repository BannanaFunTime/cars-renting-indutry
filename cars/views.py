import os

from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.http import JsonResponse, HttpResponseNotFound
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone
from cars.map_generator import MapGenerator, CarPositionGenerator
from datetime import timedelta
from .models import Auto, RentingProcess

import folium
# Create your views here.


def home(request):
    return render(request, 'cars/home.html', {})


def contact(request):
    if request.user.is_authenticated:
        context = {
            'user_authenticated': request.user.username
        }
    else:
        context = {
            'user_authenticated': None
        }
    return render(request, 'cars/contact.html', context)


def offer(request):
    cars = [{
        'id': a.id,
        'name': a.name,
        'description': a.description,
        'image': '/media/{}'.format(a.image),
        'status': a.status,
        'lat': a.lat,
        'lon': a.lon,
        'city': {
            'name': a.city.city_name,
            'lat': a.city.lat,
            'lon': a.city.lon
        }
    } for a in Auto.objects.all()]

    if request.user.is_authenticated:
        reting_confirmations = [x.once for x in RentingProcess.objects.filter(user=request.user)]
        reserved = True if True in reting_confirmations else False

        context = {
            'user_authenticated': request.user.username,
            'car_not_reserved': not reserved,
            'cars': cars
        }
    else:
        context = {
            'user_authenticated': None,
            'car_not_reserved': None,
            'cars': cars
        }

    return render(request, 'cars/offer.html', context)


def user_info(request):
    if request.user.is_authenticated:
        rentings = list(RentingProcess.objects.filter(user=request.user))

        context = {
            'user_authenticated': request.user.username,
            'rentings_active': [{
                'id': r.id,
                'auto': r.auto.name,
                'lending_timestamp': r.lending_timestamp,
                'returning_timestamp': r.returning_timestamp,
                'payment': r.calculate_payment(),
                'penalty': r.calculate_penalty(),
            } for r in rentings if not r.real_returning_timestamp],
            'rentings_unactive': [{
                'auto': r.auto.name,
                'lending_timestamp': r.lending_timestamp,
                'returning_timestamp': r.returning_timestamp,
                'real_returning_timestamp': r.real_returning_timestamp,
                'paid': r.paid,
            } for r in rentings if r.real_returning_timestamp],
        }

        return render(request, 'cars/user.html', context)

    return HttpResponseNotFound()


def city_map(request):
    map = MapGenerator('Opole')
    map.update_markers()
    map.save_adjsuted_map()

    if request.user.is_authenticated:
        context = {
            'user_authenticated': request.user.username
        }
    else:
        context = {
            'user_authenticated': None
        }
    return render(request, 'cars/city_map.html', context)


def generate_car_map(request):
    city = request.GET.get('city', None)
    car_id = request.GET.get('car_id', None)

    car_map = CarPositionGenerator(car_id=car_id, city_name=city)
    car_map.mark_car_position()
    context = {
        'map_path': '/media/html_maps/{0}'.format('car_{0}_map.html').format(car_id)
    }
    return JsonResponse(context, safe=False)


def registration(request):
    return render(request, 'cars/registration.html')


def user_registration(request):
    nick = request.POST.get('nick', None)
    password = request.POST.get('password', None)
    email = request.POST.get('email', None)

    try:
        user = User.objects.create_user(username=nick, email=email, password=password)
        user.save()

        message_fine = {
            'process_result': 0,
            'process_message': 'Użytkownik utworzony'
        }

        return JsonResponse(message_fine, safe=False)

    except Exception as exp:
        message_failed = {
            'process_result': -1,
            'process_message': 'Użytkownik {0} nie utworzony ze względu na : {1}'.format(nick, exp)
        }
        return JsonResponse(message_failed, safe=False)


def check_users_credentials(request):
    username = request.POST.get('nick')
    email = request.POST.get('email')

    used_username = User.objects.filter(username=username)
    used_email = User.objects.filter(email=email)
    message = {
        'message_dt': '',
        'valid': 0
    }

    if used_username:
        message['message_dt'] += 'Użytkownik o podanej nazwie użytkownika istnieje.\n'
    if used_email:
        message['message_dt'] += 'Adres email wdidnieje już w bazie danych.\n' \

    if not used_username and not used_email:
        message['message_dt'] = 'Podane credentails moga zostac uzyte'
        message['valid'] = 1

    return JsonResponse(message, safe=False)


def user_auth_redirection(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(request, username=username, password=password)

    if user is not None:
        login(request, user)
        message_success = {
            'message_success': "Użytkownik: {0} został poprawnie zalogowany.".format(user.username)
        }
        return JsonResponse(message_success, safe=False)

    else:
        message_failed = {
            'message_fail': "Nieprawidłowe dane logowania..."
        }
        return JsonResponse(message_failed, safe=False)


def user_logout(request):
    user_name_logout = request.user.username
    logout(request)

    message_failed = {
                'message_logout': "Użytkownik {0} został pomyślnie wylogowany".format(user_name_logout)
            }
    return JsonResponse(message_failed, safe=False)


def reservation(request):
    if not request.user.is_authenticated:
        return HttpResponseNotFound()

    car_id = request.POST.get('car_id')
    auto = Auto.objects.get(id=car_id)
    if not auto.status:
        context = {
            "taken": True
        }
        return JsonResponse(context, safe=False)
    auto.status = False
    auto.save()

    rent = RentingProcess()
    rent.auto = auto
    rent.user = request.user
    rent.lending_timestamp = timezone.now()
    rent.returning_timestamp = timezone.now() + timedelta(days=7)
    rent.once = True
    rent.save()
    context = {
        "taken": False
    }

    return JsonResponse(context, safe=False)


def finish_reservation(request):
    if not request.user.is_authenticated:
        return HttpResponseNotFound()

    rent_id = request.POST.get('rent_id')
    rent = RentingProcess.objects.get(id=rent_id)
    penalty = rent.calculate_penalty()

    rent.real_returning_timestamp = timezone.now()
    rent.paid = rent.calculate_payment() + (penalty if penalty > 0 else 0)
    rent.auto.status = True
    rent.once = False

    rent.auto.save()
    rent.save()

    return JsonResponse({}, safe=False)
