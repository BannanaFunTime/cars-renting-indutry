"""projekt URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from django.conf.urls.static import static
from django.urls import path
from cars.views import home, offer, contact, registration, user_registration, \
    user_logout, user_auth_redirection, city_map, \
    check_users_credentials, user_info, reservation, \
    finish_reservation, generate_car_map
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('home/', home, name='home'),
    path(r'', home, name='home'),
    path('registration/', registration, name='registration'),
    path('offer/', offer, name='offer'),
    path('contact/', contact, name='contact'),
    path('city_map/', city_map, name='city_map'),
    path('user/', user_info, name='user_info'),
    url(r'^ajax/logout/$', user_logout, name='user_logout'),
    url(r'^ajax/individual_car_map/$', generate_car_map, name='individual_car_map'),
    url(r'^ajax/user_auth_redirection/$', user_auth_redirection, name='user_auth_redirection'),
    url(r'^ajax/check_users_credentials/$', check_users_credentials, name='check_users_credentials'),
    url(r'^ajax/registration/$', user_registration, name='registration'),
    url(r'^ajax/reservation/$', reservation, name='reservation'),
    url(r'^ajax/finish_reservation/$', finish_reservation, name='finish_reservation'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
