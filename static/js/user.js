$(function() {
    console.log( "ready!" );

    $(".rent-finish").click(function(e) {
        var regexpFinish = /finish-(\d+)/
        var groups = regexpFinish.exec(this.id)

        $(this).text('Zakończono')
        $(this).addClass('disabled')

        $.ajax({
            url: '/ajax/finish_reservation/',
            type: "POST",
            data: {
                'rent_id': parseInt(groups[1]),
                csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
            },
            success: function (data) {
                // Do nothing
            }
        });

        $(this).unbind("click");
    });
});